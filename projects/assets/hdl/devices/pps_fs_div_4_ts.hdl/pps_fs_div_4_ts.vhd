-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
-- details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
library timed_sample_prot; use timed_sample_prot.complex_short_timed_sample.all;
architecture structural of worker is
  signal fs_div_4_generator_rstn  : std_logic := '0';
  signal generator_to_converter_tdata
      : std_logic_vector(out_out.data'length-1 downto 0) := (others => '0');
  signal generator_to_converter_tvalid : std_logic := '0';
  signal generator_to_converter_tready : std_logic := '0';
  signal arg_40_0 : std_logic_vector(39 downto 0) := (others => '0');
  signal time_valid : std_logic := '0';
  signal oclk_in_marshaller_oprotocol : protocol_t;
  signal oclk_out_adapter_irdy   : std_logic := '0';
  signal oclk_eof : std_logic := '0';
  signal oclk_data : std_logic_vector(out_out.data'range) := (others => '0');
  signal oclk_opcode : timed_sample_prot.complex_short_timed_sample.opcode_t := SAMPLE;
  signal time_seconds : std_logic_vector(31 downto 0);
  signal time_fraction : std_logic_vector(31 downto 0);

  signal time_in_fraction_r : unsigned(32-1 downto 0);
  type state_t is (IDLE, SEND_TS, SEND_SAMPLES);
  signal state : state_t;
  signal cnt : unsigned(14 downto 0);
  signal freq_is_positive : std_logic;
  signal send_zeros : std_logic;

  -- attribute mark_debug : string;
  -- attribute mark_debug of fs_div_4_generator_rstn : signal is "true";
  -- attribute mark_debug of generator_to_converter_tdata : signal is "true";
  -- attribute mark_debug of generator_to_converter_tvalid : signal is "true";
  -- attribute mark_debug of generator_to_converter_tready : signal is "true";
  -- attribute mark_debug of oclk_out_adapter_irdy : signal is "true";
  -- attribute mark_debug of oclk_eof : signal is "true";
  -- attribute mark_debug of oclk_data : signal is "true";
  -- attribute mark_debug of time_seconds : signal is "true";
  -- attribute mark_debug of time_fraction : signal is "true";
  -- attribute mark_debug of time_valid : signal is "true";
  -- attribute mark_debug of cnt : signal is "true";
  -- attribute mark_debug of freq_is_positive : signal is "true";
  -- attribute mark_debug of send_zeros : signal is "true";
begin

  -- Active when TS is sent and while sending samples..
  -- This device worker signal is routed to FPGA pin/board general-purpose output
  -- and represents the "start" of a transmition event.
  -- Ideally used to trigger a SpecA that is configured for Zero Span while
  -- monitoring the RF output.
  trig_out <= '1' when (state /= IDLE) else '0';

  sm_tx : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (ctl_in.reset = '1') then
        time_in_fraction_r      <= (others => '0');
        state                   <= IDLE;
        time_valid              <= '0';
        cnt                     <= (others => '0');
        fs_div_4_generator_rstn <= '0';  --reset sample generator
        freq_is_positive        <= '0';
        send_zeros              <= '0';
      else

        time_in_fraction_r <= time_in.fraction;

        if (oclk_out_adapter_irdy = '1') then
          case state is
            when IDLE =>
              --wait for TS to rollover
              send_zeros <= '1';
              if (time_in.fraction < time_in_fraction_r) then
                fs_div_4_generator_rstn <= '1';  --clr reset
                time_valid              <= '1';  --set for 1 cycle to send 96 bit TS
                state                   <= SEND_TS;
              end if;
            when SEND_TS =>
              --oclk_out_adapter_irdy is inactive while sending the 96 bit
              -- TS (actually, inactive for two of the three 32 bit words)
              time_valid <= '0';  --only one cycle required for initiating
              --sending of TS
              cnt        <= (others => '0');
              send_zeros <= '0';
              state      <= SEND_SAMPLES;
            when SEND_SAMPLES =>
              --Send samples for a small % of 1 sec,
              --and send zeros to "flush" the path
              --Emperically, num_zero_samples_to_send=6 has been shown to work.
              if (cnt = 4096-1) then
                cnt                     <= (others => '0');
                freq_is_positive        <= not freq_is_positive;
                fs_div_4_generator_rstn <= '0';  --set in reset
                state                   <= IDLE;
              else
                cnt <= cnt + 1;
                if (cnt = 4096 - 3 - unsigned(props_in.num_zero_samples_to_send)) then  --send amount to capture in small ILA
                  send_zeros <= '1';
                end if;
              end if;
            when others =>
          end case;
        end if;
      end if;
    end if;
  end process;

  generator_to_converter_tready <= '1' when (oclk_out_adapter_irdy = '1' and state = SEND_SAMPLES) else '0';

  generator : dsp_prims.dsp_prims.fs_div_4_generator
    port map(
      aclk             => ctl_in.clk,
      aresetn          => fs_div_4_generator_rstn,
      freq_is_positive => freq_is_positive,
      m_axis_tdata     => generator_to_converter_tdata,
      m_axis_tvalid    => generator_to_converter_tvalid,
      m_axis_tready    => generator_to_converter_tready);

  arg_40_0(39 downto 8) <= std_logic_vector(time_in.fraction);
  time_seconds          <= std_logic_vector(time_in.seconds);
  time_fraction         <= std_logic_vector(time_in.fraction);

  oclk_in_marshaller_oprotocol.sample.data.imaginary    <= (others => '0') when (send_zeros = '1') else generator_to_converter_tdata(31 downto 16);
  oclk_in_marshaller_oprotocol.sample.data.real         <= (others => '0') when (send_zeros = '1') else generator_to_converter_tdata(15 downto 0);
  oclk_in_marshaller_oprotocol.sample_vld               <= generator_to_converter_tvalid and generator_to_converter_tready;
--  oclk_in_marshaller_oprotocol.time.seconds             <= std_logic_vector(time_in.seconds + props_in.seconds_to_tx_offset); --Tx at a future time
  oclk_in_marshaller_oprotocol.time.seconds             <= std_logic_vector(time_in.seconds);  --Tx ASAP
  oclk_in_marshaller_oprotocol.time.fraction            <= arg_40_0;
  oclk_in_marshaller_oprotocol.time_vld                 <= time_valid;
  oclk_in_marshaller_oprotocol.sample_interval.seconds  <= (others => '0');
  oclk_in_marshaller_oprotocol.sample_interval.fraction <= (others => '0');
  oclk_in_marshaller_oprotocol.sample_interval_vld      <= '0';
  oclk_in_marshaller_oprotocol.flush                    <= '0';
  oclk_in_marshaller_oprotocol.discontinuity            <= '0';
  oclk_in_marshaller_oprotocol.metadata.id              <= (others => '0');
  oclk_in_marshaller_oprotocol.metadata.value           <= (others => '0');
  oclk_in_marshaller_oprotocol.metadata_vld             <= '0';

  out_marshaller : complex_short_timed_sample_marshaller
    generic map(
      WSI_DATA_WIDTH    => to_integer(OUT_PORT_DATA_WIDTH),
      WSI_MBYTEEN_WIDTH => out_out.byte_enable'length)
    port map(
      clk          => ctl_in.clk,
      rst          => ctl_in.reset,
      -- INPUT
      iprotocol    => oclk_in_marshaller_oprotocol,
      ieof         => oclk_eof,
      irdy         => oclk_out_adapter_irdy,
      -- OUTPUT
      odata        => oclk_data,
      ovalid       => out_out.valid,
      obyte_enable => out_out.byte_enable,
      ogive        => out_out.give,
      osom         => out_out.som,
      oeom         => out_out.eom,
      oopcode      => oclk_opcode,
      oeof         => out_out.eof,
      oready       => out_in.ready);

  -- this only needed to avoid build bug for xsim:
  -- ERROR: [XSIM 43-3316] Signal SIGSEGV received.
  out_out.data <= oclk_data;

  out_out.opcode <=
    complex_short_timed_sample_sample_op_e          when oclk_opcode = SAMPLE else
    complex_short_timed_sample_time_op_e            when oclk_opcode = TIME_TIME else
    complex_short_timed_sample_sample_interval_op_e when oclk_opcode = SAMPLE_INTERVAL else
    complex_short_timed_sample_flush_op_e           when oclk_opcode = FLUSH else
    complex_short_timed_sample_discontinuity_op_e   when oclk_opcode = DISCONTINUITY else
    complex_short_timed_sample_METADATA_op_e        when oclk_opcode = METADATA else
    complex_short_timed_sample_sample_op_e;

end structural;
