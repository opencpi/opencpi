#!/usr/bin/env python3

"""
Validate odata for Discontinuity Gate output.

Tests that the output file contains proper PAYLOAD bytes.
Will also validate size of packet and that all data bytes are
as expected per input file.
"""
import numpy as np, sys, os.path, struct
import sys

class color:
    RED = '\033[91m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'
    END = '\033[0m'

def validation(argv):
    payload_len = int(argv[1])
    outfile = argv[2]

    # Read in test results outfile
    with open(outfile,"rb") as file:
        result_bytes = file.read()

    #***********************
    # Verify PAYLOAD Bytes
    #***********************
    for i in range(payload_len):
        if result_bytes[i] != 170:
            print (color.RED + color.BOLD + 'FAILED: Payload Byte Match' + color.END)
            sys.exit(1)

    #***********************
    # Calculate total bytes 
    #***********************
    expected_len = payload_len
    outfile_len = len(result_bytes)
    if expected_len != outfile_len:
        print (color.RED + color.BOLD + 'FAILED: Byte Size Mismatch' + color.END)
        print('Expected size = ', expected_len)
        print('Measured size = ', outfile_len)
        sys.exit(1)

def main():
    print ("\n"+"*"*80)
    print ("*** Python: Discontinuity Gate ***")
    validation(sys.argv) 

if __name__ == '__main__':
       main()

