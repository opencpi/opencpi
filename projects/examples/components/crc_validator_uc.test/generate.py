#!/usr/bin/env python3

"""
Use this file to generate your input data.
Args: <input-file>
"""
import struct
import shutil
import numpy as np
import sys
import os.path

# Write data to a file
# Args: data type, file name, array of bytes
def write_data(file, vec):
    with open(file, 'wb') as f:
        op = 0
        line = [0, op]
        data_size = 1 # uchar
        for byte in vec:    
            line.append(byte)
            line[0] += data_size
            
        message_format = "2I" + str(len(line) - 2) + 'B'
        f.write(struct.pack(message_format , *line))
        # print("Writing: " + str(line))

def generate(argv):

    if len(argv) < 2:
        print("Exit: Enter the  CRC bits and type (e.g. 08a for CRC-8)")
        sys.exit(1)
    if len(argv) < 3:
        print("Exit: Enter an input filename")
        sys.exit(1)

    crc_type = argv[1]
    filename = argv[2]
                      
    crc_variations = {  # 4 message bytes with 3 data bytes and 1 CRC byte
                        # CRC-8
                        "08a" : np.uint8([0x8D, 0x48, 0x40, 0xAE]),
                        # CRC-8/EBU
                        "08b" : np.uint8([0x8D, 0x48, 0x40, 0xFE]),

                        # 1 message byte with 11 data bytes and 10 CRC bits
                        # CRC-10
                        "10" : np.uint8([0x8D, 0x02, 0x23]),

                        # 6 message bytes with 4 data bytes and 2 CRC bytes
                        # CRC-16/CCITT-FALSE 
                        "16a" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x72, 0x05]),
                        # CRC-16/BUYPASS
                        "16b" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0xE3, 0x6D]),
                        # CRC-16/ARC
                        "16c" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x24, 0x1B]),
                        # Zigbee with CRC-16/KERMIT
                        "16d" : np.uint8([0x61, 0x88, 0x7f, 0x3c, 0x92, 0xc7]),

                        # 14 message bytes with 11 data bytes and 3 CRC bytes
                        "24" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x20, 0x2C, 0xC3, 0x71, 0xC3, 0x2C, 0xE0, 0x57, 0x60, 0x98]),
                        
                        # 15 message bytes with 11 data bytes and 4 CRC bytes
                        # CRC-32/BZIP2
                        "32a" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x20, 0x2C, 0xC3, 0x71, 0xC3, 0x2C, 0xE0, 0xEF, 0x95, 0x79, 0xD0]),
                        # CRC-32
                        "32b" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x20, 0x2C, 0xC3, 0x71, 0xC3, 0x2C, 0xE0, 0x8C, 0x48, 0xFA, 0x50]),
                        # CRC-32C
                        "32c" : np.uint8([0x8D, 0x48, 0x40, 0xD6, 0x20, 0x2C, 0xC3, 0x71, 0xC3, 0x2C, 0xE0, 0x49, 0xAE, 0x60, 0x4B]) 
                     }

    # Write the packet data to the file
    write_data(filename, crc_variations[crc_type])

def main():
    generate(sys.argv)

if __name__ == '__main__':
    main()
