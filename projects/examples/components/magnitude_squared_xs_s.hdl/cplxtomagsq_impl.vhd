--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- On receipt of a flush opcode, applies backpressure to the input port and
-- generates an input message of `flush_length` stream samples of value zero,
-- before releasing backpressure and sending on the flush opcode. All other
-- message types passthrough undelayed.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity cplxtomagsq_impl is
 Generic (
    sampWidth   : integer );        -- OCPI Parameter    
 Port (
    smp_clk_in      : in std_logic;
    srst_in         : in std_logic;
    a_q15_in        : in std_logic_vector((sampWidth - 1) downto 0);
    b_q15_in        : in std_logic_vector((sampWidth - 1) downto 0);
    vld_in          : in std_logic;
    magsq_q15_out   : out std_logic_vector((sampWidth - 1) downto 0);
    magsq_out_vld   : out std_logic;
    magsq_out_rdy   : in std_logic );
end cplxtomagsq_impl;

architecture Behavioral of cplxtomagsq_impl is

  signal a_in : signed(a_q15_in'range);
  constant MINUS_MAXA : signed(a_q15_in'range) := ('1', others=>'0');

  signal b_in : signed(a_q15_in'range);
  constant MINUS_MAXB : signed(b_q15_in'range) := ('1', others=>'0');

  signal validMagSq   : std_logic;

begin

  -- saturate -MAX to -MAX(+1):
  a_in <= (MINUS_MAXA +1) when signed(a_q15_in) = MINUS_MAXA else signed(a_q15_in);
  b_in <= (MINUS_MAXB +1) when signed(b_q15_in) = MINUS_MAXB else signed(b_q15_in);

process(smp_clk_in)

    variable asq : signed((a_in'length+a_in'length)-1 downto 0);
    variable bsq : signed((b_in'length+b_in'length)-1 downto 0);
    variable magsq : signed((a_in'length+b_in'length)-1 downto 0);

begin
    assert a_q15_in'length = b_q15_in'length;

    if rising_edge(smp_clk_in) then
       if (srst_in = '1') then
          asq   := to_signed(0, asq'length);
          bsq   := to_signed(0, bsq'length);
          magsq := to_signed(0, magsq'length);
          magsq_out_vld <= '0';
          validMagSq <= '0';
       elsif (vld_in = '1' and magsq_out_rdy = '1') then
          magsq := (a_in*a_in) + (b_in*b_in); --< saturated inputs!
          -- We've guaranteed the inputs are symmetrical around 0, so the special
          -- case result when an input is -MAX does not result. Normalization
          -- becomes a shift+resize (in trade for saturation of a rare input value)
          magsq := shift_right(magsq, magsq_q15_out'length);
          magsq_q15_out <= std_logic_vector(resize(magsq, magsq_q15_out'length));
          magsq_out_vld <= '1';
          validMagSq <= '1';
       elsif (magsq_out_rdy = '0' and validMagSq = '1') then
          magsq_out_vld <= '1';
       elsif (magsq_out_rdy = '1' and validMagSq = '1') then
          magsq_out_vld <= '0';
          validMagSq <= '0';
       else
          magsq_out_vld <= '0';
          validMagSq <= '0';
       end if; 
    end if;
end process;

end Behavioral;
 
-- BACKGROUND:
--
-- because a twos complement reprensentation is not symetrical
-- around zero we have the special case (in fixed point math)
-- where the result needs an extra bit to properly represent the
-- full range of the signed integer portion when the Q15 value
-- is -MAX.
--
--         ** remember: QI.F * QI.F = Q(I+I).(F+F) **
--
-- therefore, if we 'saturate' the -MAX representation to -MAX(+1)
-- then we still have the Q2.30 result BUT guarantee that the
-- MSB in the integer portion will not be utilized. Normalization
-- becomes 'free' at this point.
--
-- EXAMPLE:
--
-- let's look at a Q3 and walk through the process applied above
-- for the value of a (or b) being -8 (aka the Q3 representation
-- of -MAX)
--
-- let a_in = 0b1.000 then (a_in)**2:
--
--           0b1.000 (Q1.3)  =>   0b01.000000
--         * 0b1.000 (Q1.3)  => + 0b01.000000
--         ---------         => -------------
--       0b01.000000 (Q2.6)  =>   0b10.000000
--                                  ^ this bit right here complicates
--                                  ^ follow on operations
--
-- but if we saturate to -MAX+1 then,
--
--           0b1.001 (Q1.3)  =>   0b00.110001
--         * 0b1.001 (Q1.3)  => + 0b00.110001
--         ---------         => -------------
--       0b00.110001 (Q2.6)  =>   0b01.100010
--                                  ^ guaranteed to not ovf, thus
--                                  ^ normalization is free via
--                                  ^ simple resize after shift
-- so,
--   resize(0b01.100010 >> 4) allows us to confidently use/interpret
--   the result as a properly normalized Q3 : 0b0.110
--
-- as signed integers (saturated),
--
--       (-7)  =>      +49  =>     +98
--     * (-7)  =>    + +49  =>    >> 4
--     ------  =>    -----  =>    ----
--        +49  =>      +98  =>      +6
-- (+0.765625)   (+1.53125)    (+0.750)  --> "~3/4ths of Q1.3 +ive range"
--               (| actual)    (norm'd)
--               (|   Q2.6)    (  Q1.3)
--               (+------------------------> "~3/4ths of Q2.6 +ive range"
--
--
-- YOU TRY: use the Q15 -MAX value (0x8000) to convince yourself.

