.. magnitude_squared_xs_s documentation


.. _magnitude_squared_xs_s:


Complex Magnitude Squared (``magnitude_squared_xs_s``)
======================================================
Calculates the magnitude squared of the input data stream.

Design
------
This worker calculates the magnitude squared of the input data. This is useful for doing work with
modulation schemes related to m-ary ASK signals. The reason that converting from magnitude squared
to magnitude is unnecessary lies in the fact that the m-ary slicing levels can be scaled by squaring
them to their appropriate values moving them into the magitude squared domain. This saves on complexity
and extra computations.

The mathematical representation of the implementation is given in :eq:`magnitude_squared_xs_s-equation`.

.. math::
   :label: magnitude_squared_xs_s-equation

   y[n] = x[n] * x^*[n]


In :eq:`magnitude_squared_xs_s-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

Interface
---------
.. literalinclude:: ../specs/magnitude_squared_xs_s-spec.xml
   :language: xml
   :lines: 1,19-

Opcode handling
~~~~~~~~~~~~~~~
All opcodes excluding samples are passed directly through the worker

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Implementations
---------------
.. ocpi_documentation_implementations:: ../magnitude_squared_xs_s.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``protocol_util`` primitives in sdr.assets

Limitations
-----------
Limitations of ``magnitude_squared_xs_s`` are:

 * None

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
