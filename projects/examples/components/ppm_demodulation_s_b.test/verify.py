#!/usr/bin/env python3
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import random

sys.path.insert(
    0,
    os.path.join(
        os.environ["OCPI_PROJECT_DIR"],
        "imports",
        "ocpi.examples",
        "components",
        "include",
    ),
)
from q15 import q15
import ocpi_message

# Get <Property> values from the test's XML.
try:
    sps = int(os.environ.get("OCPI_TEST_samps_per_sym"))
    leading_zero = os.environ.get("OCPI_TEST_one_has_leading_zero")
    # Handling boolean "casting"
    if "false" in leading_zero.lower():
        leading_zero = False
    else:
        leading_zero = True
except Exception as e:
    print("Could not get environment variables.\nError:  " + str(e))

# Constants
HALFSPS = int(sps / 2)

"""
@brief: sample() and non_sample() are the functions doing the work to
        check for output validity.

        'sample' opcodes are the only opcodes doing real work for
        PPM_Demod. All other opcodes should not do anything in the
        worker and leave data unchanged.

@param: input_data -- OCPI_MESSAGE object sent to the worker
        output_data-- OCPI_MESSAGE object emitted from the worker.

@ret:   Nothing if successful, will kill the whole test of unsuccessful.
"""


def sample(input_data, output_data):
    outbit = []

    for i in range(0, len(input_data) - 1, sps):  # iterate through the symbols
        first_sum = 0
        for j in input_data[i : i + HALFSPS]:
            first_sum += j

        first_avg = first_sum / HALFSPS
        second_sum = 0
        for j in input_data[i + HALFSPS : i + HALFSPS * 2]:
            second_sum += j

        second_avg = second_sum / HALFSPS

        if leading_zero == False:
            if first_avg > second_avg:
                outbit += [1]
            else:
                outbit += [0]
        else:
            if first_avg < second_avg:
                outbit += [1]
            else:
                outbit += [0]

    if len(outbit) != len(output_data.get_data()):
        print(
            "\nData length mismatch.\nOUTBIT: {}\nOUTPUT: {}".format(
                outbit, output_data
            )
        )
        sys.exit(1)

    if outbit != output_data.get_data():
        print("Data mismatch.")
        print("IN:", outbit)
        print("OUT:", output_data)
        sys.exit(1)


def main():
    # The two variables below are lists of messages.
    input_messages = ocpi_message.from_file(sys.argv[-1], "uchar", messages_in_file=True)
    output_messages = ocpi_message.from_file(
        sys.argv[-2], "uchar", messages_in_file=True
    )

    # The number of inputs should be equivalent to the number of outputs.
    # ppm_demodulation_s_b does not produce som or eom on output so the count
    # would not be correct
    if len(input_messages) is not len(output_messages):
        print("\nThe number of messages on the input and output do not match.")
        print(
            "(lengths) Input:"
            + str(len(input_messages))
            + "; Output:"
            + str(len(output_messages))
            + "\n"
        )
        sys.exit(1)

    # Iterate across all messages to test for validity.
    #for j in range(len(input_messages)):
        if (
            input_messages[j].get_opcode() == "sample"
            and output_messages[j].get_opcode() == "sample"
        ):
            sample(input_messages[j], output_messages[j])
        else:
            if input_messages[j] != output_messages[j]:
                sys.exit("Opcode Mismatch on message #{}!".format(j))


if __name__ == "__main__":
    main()
