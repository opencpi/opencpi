--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions
architecture rtl of worker is

  --Interpolates the prefix to the proper sps
  function interp (preamble : std_logic_vector(preamble'length-1 downto 0); interp_preamble_len:integer; spp:integer) 
    return std_logic_vector is
      variable preamble_out : std_logic_vector (interp_preamble_len-1 downto 0);
  begin
    for I in 0 to preamble'length-1 loop
      for J in 0 to spp-1 loop
        preamble_out(I*spp+J) := preamble(I);
      end loop;
    end loop;

    return preamble_out;
  end interp;

  signal valid_data : std_logic := '0';
  signal do_work : std_logic := '0';
  signal current_sample : std_logic := '0';
  constant samp_per_pulse : integer := to_integer(samp_per_symbol) / 2;
  constant interp_preamble_len : integer := preamble'length * samp_per_pulse;
  constant interp_preamble : std_logic_vector(interp_preamble_len - 1 downto 0) := interp(std_logic_vector(preamble), interp_preamble_len, samp_per_pulse);
  constant message_length : integer := to_integer(num_of_symbols) * to_integer(samp_per_symbol);

  signal temp_data_counter : integer := 0;
  signal temp_interp_preamble_found : std_logic := '0';
  signal temp_comparator : std_logic_vector(interp_preamble'length - 1 downto 0) := (others => '0');

begin
  do_work <= output_in.ready and input_sample_in.valid and input_average_in.valid;
  output_out.clk <= input_sample_in.clk;
  output_out.valid <= valid_data and output_in.ready;
  input_sample_out.take <= do_work;
  input_average_out.take <= do_work;
  output_out.eof <= input_sample_in.eof and input_average_in.eof and not valid_data;

  current_sample <= '1' when input_sample_in.data > input_average_in.data else '0';

  process(input_sample_in.clk)
    variable comparator : std_logic_vector(interp_preamble'length - 1 downto 0) := (others => '0');
    variable interp_preamble_found : std_logic := '0';
    variable data_counter : integer := 0;
  begin
      if rising_edge(input_sample_in.clk) then
        if input_sample_in.opcode = short_timed_sample_sample_op_e then
          if do_work = '1' then
            comparator := comparator(comparator'length - 2 downto 0) & current_sample;
            output_out.eom <= '0';
            output_out.som <= '0';

            if comparator = interp_preamble and interp_preamble_found = '0' then
              interp_preamble_found := '1';
              output_out.som <= '1';
            end if;

	    if interp_preamble_found and data_counter = 0 then
	      data_counter := data_counter + 1;
            elsif interp_preamble_found and data_counter < message_length then
              data_counter := data_counter + 1;
              output_out.data <= input_sample_in.data;
              valid_data <= '1';
            elsif interp_preamble_found and data_counter = message_length then
              output_out.eom <= '1';
              data_counter := data_counter + 1;
              output_out.data <= input_sample_in.data;
              valid_data <= '1';
            else
              data_counter := 0;
              valid_data <= '0';
              interp_preamble_found := '0';
            end if;
          elsif output_in.ready and valid_data then
             valid_data <= '0';
          end if;
        end if;
      end if;
      temp_data_counter <= data_counter;
      temp_comparator <= comparator;
      temp_interp_preamble_found <= interp_preamble_found;
  end process;

end rtl;
