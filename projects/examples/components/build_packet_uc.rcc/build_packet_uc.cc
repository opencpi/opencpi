//*****************************************************************************
// build_packet_uc-rcc
//
// This worker supports the OpenCPI FSK demonstration application. It receives
// samples from the file_read.rcc worker which currently does not support the
// timed-sample interface protocol. When an OpenCPI user runs the application,
// this worker first outputs a sequence of HEADER bytes followed by the SAMPLE
// bytes received from the file_read worker.  This worker continues to output
// sample bytes until it receives the end-of-file (EOF) signal from the
// file_read worker. Once the worker receives the EOF signal, it sends a
// sequence of TAIL bytes to indicate end of packet.    
//
//	Interfaces:
//		input: uint8_t port
//	       output: uchar_time_sample-prot 
//
// The 'bypass_done' property allows component to be reset to continue
// processing new input data once the tail pattern has been sent for previous
// data.  If set to 'false', then component only processes one set of data
// input and sets application state to 'RCC_DONE'. This feature supports a one
// -shot transmission of data without need to set the time argument for
// running the example FSK application.  [ NOT FULLY IMPLEMENTED YET]   
//
// ****************************************************************************

#include "build_packet_uc-worker.hh"
#include <vector>
#include <unistd.h>

using namespace OCPI::RCC; // for easy access to RCC data types and constants
using namespace Build_packet_ucWorkerTypes;

class Build_packet_ucWorker : public Build_packet_ucWorkerBase {

    /* Controls sequence of packet build.  Only one set true at a time */
    bool add_sync_header = true;
    bool add_payload = false;
    bool first_payload = true;
    bool add_tail = false;
    int sync_len = 0;
    int header_len = 8;
    int tail_len = 8;
    bool done = false;
    int count = 0;

  RCCResult run(bool /*timedout*/) {
       uint8_t* outData = output.sample().data().data();
       const uint8_t *inputData = input.sample().data().data();

       count++;

       /************************************************************************
	*  Detect EOF
	*
	*  Detect end-of-file (EOF) to complete packet build.
	************************************************************************/
       if (input.eof() && !done) {
          printf("build_packet_uc : EOF received\n");
          add_payload = false;
          add_tail = true;
       }

      /************************************************************************
       *  ADD SYNC BYTES
       *
       *  The syncronization bytes are required to stabilize the receiver demod.
       *  THis required number may vary dependent of the rate.  The higher the
       *  rate, more bytes are required.
       ************************************************************************/
       if (add_sync_header){
	   sync_len = properties().num_of_sync_bytes;
	   for (int i=0; i < sync_len; i++){
	     outData[i] = 0x55;
	   }

      /************************************************************************
       *  ADD HEADER BYTES
       *
       *  Send HEADER bytes to the output port. This is the start of the
       *  packet. The header bytes are uses to perform intial RX synchronozation
       *  and for RX to identify the start of the received packet.
       ************************************************************************/
	   // Load header bytes from properties.
	   uint64_t PACKET_HEADER = properties().header_bytes;
           output.sample().data().resize(sync_len+header_len);

           for (int i = header_len-1; i >= 0; i--){
               outData[sync_len+ header_len-1-i] = (PACKET_HEADER >> i*8) & 0xFF;
	   }

	   add_payload = true;
	   add_sync_header = false;          

	   if(input.opCode() == Uchar_timed_sampleSample_OPERATION){
               int input_length = input.sample().data().size();
               int payload_start = sync_len+header_len;
               output.sample().data().resize(sync_len+header_len+input_length);
               for (int i = 0; i < input_length; i++){
                   outData[payload_start+i] = *inputData;
                   inputData++;
              }
           }
	   return RCC_ADVANCE;
       }
       /************************************************************************
        *  ADD PAYLOAD BYTES
        *
        *  Payload bytes are recevied on the input 'port'. For the FSK example
        *  app, the payload is sent by the file_read component. The payload app
        *  is complete when an end-of-file (EOF) is received on the port. 
        ***********************************************************************/
       if (add_payload){
           if(input.opCode() == Uchar_timed_sampleSample_OPERATION){
	       int input_length = input.sample().data().size();
               output.sample().data().resize(input_length);
               for (int i = 0; i < input_length; i++){
                  outData[i] = *inputData;
                  inputData++;
               }
               return RCC_ADVANCE;
           }
       }
      /*************************************************************************
       *  ADD TAIL BYTES
       *
       *  When payload is complete following receiving an EOF signal, add TAIL
       *  bytes to end of packet.     
       ************************************************************************/
       if (add_tail){
           uint64_t PACKET_TAIL = properties().tail_bytes;
           for (int i = tail_len-1; i >= 0; i--){
               outData[tail_len-1-i] = (PACKET_TAIL >> i*8) & 0xFF;
           }

           // Add tail sync bytes to flush out signal through path
	   int tail_sync_len = sync_len;
           for (int i = tail_len; i < tail_len+tail_sync_len; i++){
               outData[i] = 0x55;
           }


	   output.sample().data().resize(tail_len + tail_sync_len);
	   output.advance();
	   output.setEOF();
	   output.advance();
           add_tail = false;
	   done = true;
           return RCC_OK;
       }
       //Need to wait until TX path is flushed before sending EOF
       if (done == true){
           count++;
           if (count == 1000) {
               return RCC_DONE;
           }
       }
      return RCC_OK;
   }
};

BUILD_PACKET_UC_START_INFO
BUILD_PACKET_UC_END_INFO
