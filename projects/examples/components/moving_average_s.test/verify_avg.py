#!/usr/bin/env python3
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import sys
import os
import math
sys.path.insert(0, os.path.join(os.environ['OCPI_PROJECT_DIR'], 'imports', 'ocpi.sdr.assets', 'components', 'include'))

from q15 import q15
import ocpi_message


if __name__ == '__main__':
    length = int(os.environ.get("OCPI_TEST_length"))
    scale = q15(32768 / length * 2**-15) 

    input_message = ocpi_message.from_file(sys.argv[-1], 'real', messages_in_file=True)
    output_message = ocpi_message.from_file(sys.argv[-2], 'real', messages_in_file=True)

    message = ocpi_message.ocpi_message()
    message.set_protocol('real')
    avg_sum = 0

    full_input_message = ocpi_message.ocpi_message()
    full_input_message.set_protocol('real')
    for i in input_message:
        for j in range(i.length()):
            full_input_message.append(i[j])

    full_output_message = ocpi_message.ocpi_message()
    full_output_message.set_protocol('real')
    for i in output_message:
        for j in range(i.length()):
            full_output_message.append(i[j])

    for i in range(full_input_message.length()):
        if i < length - 1:
            avg_sum += full_input_message[i]
        else:
            avg_sum += full_input_message[i]
            appended_q15 = math.floor((float(avg_sum) * float(scale)) * 32768) / 32768.0
            message.append(q15(appended_q15))
            avg_sum -= full_input_message[i - length + 1]

    if full_output_message != message:
        exit(-1)
    exit(0)

    # sum = 0

    # out = np.zeros(max_iter, dtype = data_type)
    # it = length+3

    # if all(idata == 0):
    #     print('generate values are all 0, exiting...')
    #     sys.exit(1)

    # if all(odata == 0):
    #     print('values from block are all 0, exiting...')
    #     sys.exit(1)

    # for i in range(4, length+3):
    #     sum = sum + idata[i]

    # for i in range(0, max_iter):
    #     sum = sum + idata[it]
    #     out[i] = sum*scale
    #     sum = sum - idata[i+4]
    #     it = it + 1

    # verify(out,odata,max_iter)
