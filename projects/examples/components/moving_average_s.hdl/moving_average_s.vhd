-- moving_average_s HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE; use IEEE.std_logic_1164.all; use ieee.numeric_std.all; use ieee.math_real.all;
library ocpi; use ocpi.types.all; -- remove this to avoid all ocpi name collisions

architecture rtl of worker is
  signal valid_data : std_logic := '0';
  signal do_work : std_logic := '0';

  constant len : integer := to_integer(length);
  constant len_bits : natural := integer(ceil(log2(real(len))));
  constant datawidth : integer := input_in.data'length;
  constant scale : signed(datawidth - 1 downto 0) := to_signed(natural(ceil(1.0 / real(len) * real(2**(datawidth-1)))), datawidth);
begin
   do_work <= output_sample_in.ready and output_average_in.ready and input_in.valid;
   output_sample_out.valid <= valid_data and output_sample_in.ready;
   output_average_out.valid <= valid_data and output_average_in.ready;
   input_out.take <= do_work;
   output_sample_out.eof <= input_in.eof and not valid_data;
   output_average_out.eof <= input_in.eof and not valid_data;
   output_sample_out.eom <= input_in.eom;
   output_average_out.eom <= input_in.eom;
   output_sample_out.som <= input_in.som;
   output_average_out.som <= input_in.som;

  process(ctl_in.clk)
    type shift_register_t is array (len - 1 downto 0) of signed(15 downto 0);

    variable samples : shift_register_t := (others => (others => '0'));
    variable start_up_counter : integer range 0 to len - 1 := 0;
    variable accumulator : signed((input_in.data'left + len_bits) downto 0) := (others => '0');
    variable average : signed((accumulator'length + datawidth - 1) downto 0) := (others => '0');

    begin
       if rising_edge(ctl_in.clk) then
          if input_in.opcode = short_timed_sample_sample_op_e then
             if do_work = '1' then
                accumulator := accumulator + resize(signed(input_in.data), accumulator'length) - resize(samples(0), accumulator'length);
                samples := signed(input_in.data) & samples(len - 1 downto 1);

                valid_data <= '0';
                output_sample_out.data <= std_logic_vector(samples(0));

                if (start_up_counter < len -  1) then
                    start_up_counter := start_up_counter + 1;
                else
                    average := accumulator * scale;
                    average := shift_right(average, (accumulator'length - 1) - len_bits);

                    output_average_out.data <= std_logic_vector(resize(average, output_average_out.data'length));
                    valid_data <= '1';
                end if;

             elsif output_sample_in.ready and valid_data then
                valid_data <= '0';
             end if;
          end if;
       end if;
    end process;

end rtl;
