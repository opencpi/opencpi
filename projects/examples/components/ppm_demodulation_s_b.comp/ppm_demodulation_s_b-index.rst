.. ppm_demodulation_s_b documentation


.. _ppm_demodulation_s_b:


PPM Demodulator (``ppm_demodulation_s_b``)
===========================================
PPM (Pulse Position Modulation) demodulation

Design
------
Pulse Position Modulation (PPM) is a technique that modulates a carrier signal on and off over a duration of symbol time.  PPM is similar to on-off keying (OOK) but is slightly different because the interpreted bit value is dependent on when the pulse within the designated symbol period is on or off.  If ON in first half of symbol and OFF in second half is interpreted as a binary ‘1’, then when first half of symbol is OFF  and second half is ON, then the binary value will be interpreted as ‘0’.   The opposite case is an option for this component as well.  The “one_has_leading_zeros” property is used to select either case as shown in :numref:`ppm_demodulation_s_b-diagram`.

.. _ppm_demodulation_s_b-diagram:

.. figure:: ppm_demodulation_s_b.svg
   :alt: Skeleton alternative text.
   :align: center

   PPM Symbols to bit values.

The correct interpretation is highly dependent on the accurate alignment of the input samples.
The component presumes the first sample of PPM input stream is aligned with the start of 
message (SOM).

Interface
---------
.. literalinclude:: ../specs/ppm_demodulation_s_b-spec.xml
   :language: xml
   :lines: 1,19-

Opcode Handling
~~~~~~~~~~~~~~~
Component only passes sample opcodes.

Ports
~~~~~
.. ocpi_documentation_ports::
   input: PPM pulses in. Assumed to be aligned to input message boundary.
   output: Demodulated PPM binary data.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Implementations
---------------
.. ocpi_documentation_implementations:: ../ppm_demodulation_s_b.hdl

Example Application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:
 None

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``


Limitations
-----------
Limitations of ``ppm_demodulation_s_b`` are:

 * Input samples must be aligned with the proper pulse position based on start of message.

Testing
-------
.. ocpi_documentation_test_platforms::

.. ocpi_documentation_test_result_summary::
