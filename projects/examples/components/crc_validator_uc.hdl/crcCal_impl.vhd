-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity crcCal_impl is 
   Generic (
       CRCdataWidth   : integer;
       CRCbitWidth    : integer;
       polyValue      : in integer  );
   Port ( 
       clk            : in std_logic; 
       reset          : in std_logic;
       data           : in std_logic_vector((CRCdataWidth - 1) downto 0); 
       data_valid     : in std_logic;
       message        : out std_logic_vector((CRCdataWidth - 1) downto 0);
       message_vld    : out std_logic;
       crcStart       : in std_logic;
       crcDone        : out std_logic );
end crcCal_impl;

architecture Behavioral of crcCal_impl is    
   constant    maxPolySize    : integer := 32;
   
   signal  poly        : std_logic_vector((maxPolySize - 1) downto 0) := std_logic_vector(to_unsigned(polyValue, maxPolySize));
   signal  crc         : std_logic_vector((CRCbitWidth - 1) downto 0) := (others => '1'); -- Final CRC output

begin

crcCal: process(clk)

   variable v          : std_logic_vector((CRCdataWidth - 1) downto 0) := (others => '0');
   variable u          : std_logic_vector(CRCbitWidth downto 0) := (others => '0');
   variable w          : std_logic_vector(CRCbitWidth downto 0) := (others => '0');
   variable y          : std_logic_vector(CRCbitWidth downto 0) := (others => '0');
   variable f          : std_logic_vector((CRCbitWidth - 1) downto 0) := (others => '0');
   variable i,j,x, z   : integer := 0;
  
begin 

   if (rising_edge(clk)) then
       if (reset = '1') then
           crc         <= (others => '1');
           message     <= (others => '0');
           message_vld <= '0';
           crcDone     <= '0';
       elsif (crcStart = '1') then 
           if (data_valid = '1') then
               v((CRCdataWidth - 1) downto 0):= data((CRCdataWidth - 1) downto 0); --Changed for the Receiver:  Entire Transmitted Message with CRC
               u := poly(CRCbitWidth downto 0); 
               w := v((CRCdataWidth - 1) downto ((CRCdataWidth - CRCbitWidth) -1)); --111 bits downto 87 bits
               for i in ((CRCdataWidth - CRCbitWidth) -1) downto 0 loop --88 bits Shifting loop for the XOR operation and remainder
                   if(w(CRCbitWidth)='1') then --24th bits
                       w:=w xor u;
                   else
                       null;
                   end if;
                   y := w;
                   w(CRCbitWidth downto 1) := y((CRCbitWidth - 1) downto 0); --24 downto 1 and 23 downto 0 Changed for Experiment: Shift
                   if(i=0) then
                       w(0) := '0';
                   else
                       w(0) := v(i-1);
                   end if;
               end loop;
               f := w((CRCbitWidth - 1) downto 0); --25bit slice of V (entire message) --Changed for Receiver
               if (f = (f'range => '0')) then
                   crc<=w((CRCbitWidth - 1) downto 0); -- 23 downto 0 redundant bits
                   message <= data((CRCdataWidth - 1) downto 0); --111 bit downto 24 bit Changed for Receiver: First Part of Message without CRC
                   message_vld <= '1'; 
                   crcDone <= '1';
               else
                   crc<=w((CRCbitWidth - 1) downto 0); --23 downto 0 incorrect redundant bits
                   message <= (others => '0'); --Changed for Receiver: First Part of Message without CRC
                   message_vld <= '0';
                   crcDone <= '1';                  
               end if;
           end if;
       else 
             crcDone <= '0';
       end if;
   end if;
end process;

end Behavioral;

