-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.STD_LOGIC_1164.ALL;
use IEEE.NUMERIC_STD.ALL;

entity wordUnpack_impl is
   Generic(
       CRCmessageWidth  : integer;
       messageWidth     : integer;
       numOutWords      : integer );
   Port (clk            : in std_logic;
       reset            : in std_logic;
       input            : in std_logic_vector((CRCmessageWidth - 1) downto 0);
       vld_in           : in std_logic;
       eof_in           : in std_logic;
       output           : out std_logic_vector((messageWidth - 1) downto 0);
       output_vld       : out std_logic;
       output_rdy       : in std_logic;
       som              : out std_logic;
       eom              : out std_logic;
       eof_out          : out std_logic;
       numOutputs       : out std_logic_vector(31 downto 0);
       unpackStart      : in std_logic;
       unpackDone       : out std_logic);
end wordUnpack_impl;

architecture Behavioral of wordUnpack_impl is
    
    signal  wordCtr         : unsigned(7 downto 0) := x"00";
    signal  wordFlg         : std_logic;
    signal  numOutputCtr    : unsigned(31 downto 0);
    signal  somFlg          : std_logic;
    signal  eomFlg          : std_logic;

begin  

outputData:process(clk)    
begin
   if (rising_edge(clk)) then
       if (reset = '1') then
           output     <= (others => '0');
           output_vld <= '0';
           wordFlg    <= '0';
           wordCtr    <= (others => '0');
           unpackDone <= '0';
       elsif (unpackStart = '1' and vld_in = '1') then
          if (output_rdy = '1') then
               if (wordCtr <  (numOutWords)) then
                   output <= input(((CRCmessageWidth - 1) - to_integer(wordCtr * messageWidth)) downto 
                      (((CRCmessageWidth - 1) - to_integer(wordCtr * messageWidth)) - (messageWidth - 1)));
                   output_vld <= '1';
                   wordFlg <= '1';
                   wordCtr <= wordCtr + 1;
                   unpackDone <= '0';
               else
                   output <= (others => '0');
                   output_vld <= '0';
                   wordFlg <= '0';
                   unpackDone <= '1';
               end if;
           end if;
       else
           output <= (others => '0');
           output_vld <= '0';
           wordFlg <= '0';
           wordCtr <= (others => '0');
           if (unpackStart = '1') then
               unpackDone <= '1'; 
           else
               unpackDone <= '0';
          end if;
       end if;
   end if;
end process;

outputCt:process(wordFlg, reset)
begin
   if (reset = '1') then
       numOutputCtr <= (others=> '0');
   elsif (rising_edge(wordFlg)) then
       numOutputCtr <= numOutputCtr + 1;
   end if;
   numOutputs <= std_logic_vector(numOutputCtr);
end process;

metaData:process(clk)
begin
   if (rising_edge(clk)) then
       if (reset = '1') then
           som <= '0';
           eom <= '0';
           somFlg <= '0';
           eomFlg <= '0';
           eof_out <= '0';
       elsif (unpackStart = '1' and vld_in = '1') then 
           if (output_rdy = '1') then
               if (wordCtr = 0 and somFlg = '0') then
                   som <= '1';
                   somFlg <= '1';
               elsif (wordCtr = (numOutWords - 1) and eomFlg = '0') then
                   eom <= '1';
                   eomFlg <= '1';
               else
                   som <= '0';
                   eom <= '0';
               end if;
           end if;
       else
           somFlg <= '0';
           eomFlg <= '0';
           if (eof_in = '1') then
               eof_out <= '1';
           end if;
       end if;
end if;
end process;

end Behavioral;

