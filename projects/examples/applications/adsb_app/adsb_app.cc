//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.
//
// *****************************************************************************

#include <iostream>
#include <unistd.h>
#include <cstdio>
#include <cassert>
#include <string>
#include "OcpiApi.hh"
#include <csignal>
#include <sstream>
#include <iomanip>
#include <string>
#include <vector>
#include <time.h>

#include <sys/socket.h>
#include <arpa/inet.h>
#include <getopt.h>
#include "adsb_app.hh"

namespace OA = OCPI::API;

volatile sig_atomic_t signal_received = 0;
void signal_handler(int signum) { signal_received = signum; }

typedef unsigned char byte;

std::string ToHex(const byte* buffer, size_t size){
    std::stringstream str;
    str.setf(std::ios_base::hex, std::ios::basefield);
    str.setf(std::ios_base::uppercase);
    str.fill('0');
    str << "*";	
    for(size_t i=0; i<size; ++i){
        str << std::setw(2) << (unsigned short)(byte)buffer[i];
    }
    str << ";\r\n";
    return str.str();
}

int32_t main(int32_t argc, char **argv) {
  // ***************************************************************************
  //  Process application options.
  // ***************************************************************************
  int32_t opt;
  char *arg_long = nullptr;
  int32_t option_index;
  timespec current_time;
  
  // Process option, if not defined, then application will use default values
  while ((opt = getopt_long(argc, argv, "h", long_options, &option_index)) !=
         -1) {
    switch (opt) {
      case 0: {
        arg_long = optarg;
        if (strcmp(long_options[option_index].name, "verbose") == 0) {
            verbose_flag = 1; 
        }
        if (strcmp(long_options[option_index].name, "display") == 0) {
           display_flag = 1;
        }
        if (strcmp(long_options[option_index].name, "timeout") == 0) {
          timeout = atoi(arg_long);
        }

        break;
      }
      // Process option help request.  Display option usage.
      case 'h': {
        usage();
        break;
      }
      case '?': {
        std::cout << "\nGot unknown option." << std::endl;
        usage();
        break;
      }
      default: {
        std::cout << "\nGot unknown parse returns: " << opt << std::endl;
        usage();
      }
    }
  }

  //***************************************************************************

   int packet_count = 0;  

   // For an explanation of the ACI, see:
   // https://opencpi.gitlab.io/releases/develop/docs/OpenCPI_Application_Development_Guide.pdf
   signal(SIGINT, signal_handler);
   if (timeout < 0){
   	std::cout << "Press Ctrl-C to interrupt...\n";
   }
   else {
	std::cout << "App will timeout in " << (int)timeout << " seconds or press Ctrl-C to exit.\n";
   }

   int sockfd; 
   struct sockaddr_in server_addr;

   try {
      sockfd = socket(AF_INET, SOCK_STREAM, 0); 
      if (sockfd == -1) {
         std::cerr << "Error creating socket" << std::endl;
         return 1;
   }
   // Set Server address
   server_addr.sin_family = AF_INET;
   server_addr.sin_port = htons(30001);
   if (inet_pton(AF_INET, "127.0.0.1", &server_addr.sin_addr) <= 0) {
       std::cerr << "Error converting address" << std::endl;
       return 1;
   }
   // Connect to Server
   if (connect(sockfd, (struct sockaddr *)&server_addr, sizeof(server_addr)) == -1 ) {
       std::cerr << "Error connecting" << std::endl;
       return 1;
   }

   std::vector<OA::PValue> params_vec;
   params_vec.push_back(OA::PVBool("verbose", verbose_flag));
   params_vec.push_back(OA::PVEnd);

   OA::PValue *params = &params_vec[0];

   OA::Application app("adsb_app.xml", params);
   app.initialize(); // all resources have been allocated
   OA::ExternalPort &ep = app.getPort("output");
   app.start();      // execution is started

   OA::ExternalBuffer *b;

   while(!signal_received) {
       uint8_t *data;
       size_t length;
       uint8_t opcode;
       bool end;
       if ((b = ep.getBuffer(data, length, opcode, end))) {
           b->release();
           // Publish a message
	   packet_count++;
	   std::string str;
	   str = ToHex(data,length);

	   clock_gettime(CLOCK_PROCESS_CPUTIME_ID, &current_time);
	   
	   if (timeout >= 0){
		   if ( (int)current_time.tv_sec >= timeout) {
			   return 1;
		   }
	   }
	   // Display packets to terminal output
           if(display_flag){
	       printf("To tar1090[%d]: ", packet_count);
	       for (int i = 0; i < (int)length; i++){
	           printf("%x ",data[i]);
	       }
	       printf("\n");
	   }
	   // Send packet to tar1090 site
	   int bytes_sent = send(sockfd, str.c_str(), str.length(), 0);
           if (bytes_sent == -1) {
	       std::cerr << "Error sending data" << std::endl;
               return 1;
           }
       }
   }
   app.finish();     // do end-of-run processing like dump properties
   close(sockfd);
   } catch (std::string &e) {
       std::cerr << "app failed: " << e << std::endl;
       return 1;
   }
   return 0;
}
