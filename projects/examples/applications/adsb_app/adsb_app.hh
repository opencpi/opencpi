//
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <string.h>

// *********************************************************************
// Initiate default property values for executing adsb_app ACI
// application.
// *********************************************************************
int32_t timeout          = -1;
double rx_gain           = 0.0;
std::string rx_gain_mode = "auto";

// *********************************************************************
// Defines options to be used for application execution using <getopt.h>
// *********************************************************************
int32_t display_flag = 0;
int32_t verbose_flag = 0;
static struct option long_options[] = {
    {"help", no_argument, 0, 'h'},
    {"display", no_argument, &display_flag, 1},
    {"verbose", no_argument, &verbose_flag, 1},
    {"rx-gain", required_argument, 0, 0},
    {"rx-gain-mode", required_argument, 0, 0},
    {"timeout", required_argument, 0, 0},
    {0, 0, 0, 0}};

// *********************************************************************
// Displays usage for configurable application properties.  Called using
// (--help) or when option error detected.
// *********************************************************************
void usage() {
  printf("\nOptions:\n");
  printf(" --help                Display options\n");
  printf(" --verbose             Show ocpirun debug\n");
  printf(" --display             Display ADSB packets\n");
    printf(
      " --rx-gain             Receive gain =(dB), [default = 0.0]\n");
  printf(
      " --rx-gain-mode        {'manual' or 'auto', [default = 'auto']\n");
  printf(" --timeout             Duration to run application (seconds), [default "
      "= -1] (no timeout)\n");
  printf("\n");
  exit(1);
}

