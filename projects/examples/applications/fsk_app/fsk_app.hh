//
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include <string.h>

// *********************************************************************
// Initiate default property values for executing fsk_app_rcc ACI
// application.
// *********************************************************************
std::string mode         = "txrx";
std::string model        = "rcc";
std::string rcc_platform = "rcc0";
std::string hdl_platform = "e31x";
std::string tx_filename  = "idata/opencpi.jpg";
std::string rx_filename  = "odata/opencpi.jpg";
double tx_frequency      = 2450.0;
double rx_frequency      = 2450.0;
double tx_sample_rate    = 0.25;
double rx_sample_rate    = 0.25;
double tx_bandwidth      = 0.25;
double rx_bandwidth      = 0.25;
double tx_offset         = 0.0;
double rx_offset         = 0.0;
float tx_gain            = 0.0;
std::string rx_gain      = "auto";
int32_t timeout          = -1;

// *********************************************************************
// Defines options to be used for application execution using <getopt.h>
// *********************************************************************
int32_t display_flag = 0;
int32_t verbose_flag = 0;
static struct option long_options[] = {
    {"help", no_argument, 0, 'h'},
    {"display", no_argument, &display_flag, 1},
    {"verbose", no_argument, &verbose_flag, 1},
    {"mode", required_argument, 0, 0},
    {"rcc-platform", required_argument, 0, 0},
    //{"hdl-platform", required_argument, 0, 0},
    {"model", required_argument, 0, 0},
    {"tx-freq", required_argument, 0, 0},
    {"rx-freq", required_argument, 0, 0},
    {"tx-offset", required_argument, 0, 0},
    {"rx-offset", required_argument, 0, 0},
    {"tx-filename", required_argument, 0, 0},
    {"rx-filename", required_argument, 0, 0},
    {"tx-rate", required_argument, 0, 0},
    {"rx-rate", required_argument, 0, 0},
    {"tx-bandwidth", required_argument, 0, 0},
    {"rx-bandwidth", required_argument, 0, 0},
    {"tx-gain", required_argument, 0, 0},
    {"rx-gain", required_argument, 0, 0},
    {"timeout", required_argument, 0, 0},
    {0, 0, 0, 0}};

// *********************************************************************
// Displays usage for configurable application properties.  Called using
// (--help) or when option error detected.
// *********************************************************************
void usage() {
  printf("\nOptions:\n");
  printf(" --help                Display options\n");
  printf(" --display             Show final configuration properties\n");
  printf(" --verbose             Show ocpirun debug\n");
  printf(" --mode                Values = {txrx, tx, rx}, [default = txrx]\n");
  printf(
      " --rcc-platform        Defines which RCC platform to run RCC components, "
      "[default = rcc0]\n");
  //printf(
  //    " --hdl-platform        Defines which HDL platform to run HDL "
  //    "components, [default = e31x]\n");
  printf(
      " --model               Select authoring model {rcc, hdl}, [default = "
      "rcc]\n");
  printf(
      " --tx-freq             Transmit carrier frequency (MHz), [default = "
      "2450.0]\n");
  printf(
      " --rx-freq             Receive carrier frequency (MHz),  [default = "
      "2450.0]\n");
  printf(
      " --tx-offset           Transmit frequency offset (MHz), [default = "
      "0.0]\n");
  printf(
      " --rx-offset           Receive frequency offset (MHz),  [default = "
      "0.0]\n");
  printf(
      " --tx-filename         Name of file to send, [default = idata/opencpi.jpg]\n");
  printf(
      " --rx-filename         Name of file to save received data, [default = "
      "odata/opencpi.jpg]\n");
  printf(
      " --tx-rate             Transmit sample rate (Msps), [default= 0.25]\n");
  printf(
      " --rx-rate             Receive sample rate (Msps),  [default = 0.25]\n");
  printf(
      " --tx-bandwidth        Transmit 3dB bandwidth (MHz),[default = 0.25]\n");
  printf(
      " --rx-bandwidth        Receive 3dB bandwidth (MHz), [default = 0.25]\n");
  printf(
      " --tx-gain             Transmit gain = gain_dB (dB), [default = "
      "-0.0]\n");
  printf(
      " --rx-gain             Receive gain =  {'auto, 'gain_dB'} (dB)}, "
      "[default = auto]\n");
  printf(
      " --timeout             Duration to run application (seconds), [default "
      "= -1] (no timeout)\n");
  printf("\n");
  exit(1);
}

// *********************************************************************
// Displays final option values if asked using (--display) option
// *********************************************************************

// Display txrx properties
void display_txrx_properties() {
  printf("\nDefined options:\n");
  printf("   mode           = %s\n", mode.c_str());
  printf("   model          = %s\n", model.c_str());
  printf("   rcc platform   = %s\n", rcc_platform.c_str());
  //printf("   hdl platform   = %s\n", hdl_platform.c_str());

  if (timeout < 0)
    printf("   timeout        = none\n");
  else
    printf("   timeout        = %d (sec)\n", timeout / 1000000);
  printf("   tx_frequency   = %0.3f (MHz)\n", tx_frequency);
  printf("   tx_offset      = %0.3f (MHz)\n", tx_offset);
  printf("   tx_sample_rate = %0.3f (Msps)\n", tx_sample_rate);
  printf("   tx_bandwidth   = %0.3f (MHz)\n", tx_bandwidth);
  printf("   tx_gain        = %0.1f (dB)\n", tx_gain);
  printf("   tx_filename    = %s\n", tx_filename.c_str());
  printf("   rx_frequency   = %0.3f (MHz)\n", rx_frequency);
  printf("   rx_offset      = %0.3f (MHz)\n", rx_offset);
  printf("   rx_sample_rate = %0.3f (Msps)\n", rx_sample_rate);
  printf("   rx_bandwidth   = %0.3f (MHz)\n", rx_bandwidth);
  printf("   rx_gain        = %s\n", rx_gain.c_str());
  printf("   rx_filename    = %s\n\n", rx_filename.c_str());

  return;
}

// Display tx only properties
void display_tx_properties() {
  printf("\nDefined options:\n");
  printf("   mode           = %s\n", mode.c_str());
  printf("   model          = %s\n", model.c_str());
  printf("   rcc platform   = %s\n", rcc_platform.c_str());
  //printf("   hdl platform   = %s\n", hdl_platform.c_str());
  if (timeout < 0)
    printf("   timeout        = none\n");
  else
    printf("   timeout        = %d (sec)\n", timeout / 1000000);
  printf("   tx_frequency   = %0.3f (MHz)\n", tx_frequency);
  printf("   tx_offset      = %0.3f (MHz)\n", tx_offset);
  printf("   tx_filename    = %s\n", tx_filename.c_str());
  printf("   tx_sample_rate = %0.3f (Msps)\n", tx_sample_rate);
  printf("   tx_bandwidth   = %0.3f (MHz)\n", tx_bandwidth);
  printf("   tx_gain        = %0.3f (dB)\n", tx_gain);
  return;
}

// Display rx only properties
void display_rx_properties() {
  printf("\nDefined options:\n");
  printf("   mode           = %s\n", mode.c_str());
  printf("   model          = %s\n", model.c_str());
  printf("   rcc platform   = %s\n", rcc_platform.c_str());
  //printf("   hdl platform   = %s\n", hdl_platform.c_str());

  if (timeout < 0)
    printf("   timeout        = none\n");
  else
    printf("   timeout        = %d (sec)\n", timeout / 1000000);
  printf("   rx_frequency   = %0.3f (MHz)\n", rx_frequency);
  printf("   rx_offset      = %0.3f (MHz)\n", rx_offset);
  printf("   rx_filename    = %s\n", rx_filename.c_str());
  printf("   rx_sample_rate = %0.3f (Msps)\n", rx_sample_rate);
  printf("   rx_bandwidth   = %0.3f (MHz)\n", rx_bandwidth);
  printf("   rx_gain        = %s\n", rx_gain.c_str());

  return;
}

// *********************************************************************
// Check that specified RCC and HDL platforms are valid
// *********************************************************************

// If desired and supported rcc platform not in list, maybe added here.
int32_t NUM_RCC_PLATFORMS = 5;
std::string RCC_PLATFORM[] = {"rcc0", "rcc1", "rcc2", "rcc3", "rcc4"};

bool check_rcc_platform(std::string test_rcc_platform) {
  for (int32_t i = 0; i < NUM_RCC_PLATFORMS; i++) {
    if (test_rcc_platform == RCC_PLATFORM[i]) {
      return true;
    }
  }
  return false;
}

// If desired and supported hdl platform not in list, maybe added here.
int32_t NUM_HDL_PLATFORMS = 3;
std::string HDL_PLATFORM[] = {"e31x", "plutosdr", "zed", "zcu104"};

bool check_hdl_platform(std::string test_hdl_platform) {
  for (int32_t i = 0; i < NUM_HDL_PLATFORMS; i++) {
    if (test_hdl_platform == HDL_PLATFORM[i]) {
      return true;
    }
  }
  return false;
}
