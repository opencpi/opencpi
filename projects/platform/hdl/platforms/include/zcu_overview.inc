.. Common Xilinx ZCUxxx system overview

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.

How to Use This Document
------------------------

This document provides installation information that is specific
to the OpenCPI Xilinx\ |reg| Zynq\ |reg| UltraScale+\ |trade| |device_name|.

Use this document when configuring the |device_name| hardware for OpenCPI and
when performing the tasks described
in the chapter "Enabling OpenCPI Development for Embedded Systems"
in the 
`OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_.
This document supplies details about enabling OpenCPI development on the |device_name|
that can be applied to the procedures described in the referenced *OpenCPI Installation Guide* chapter.
The recommended method is to have the *OpenCPI Installation Guide* and this document
open in separate windows and refer to this document for any platform-specific details
while following the OpenCPI setup tasks described in the Installation Guide.

The following documents can also be used as references for the tasks described in this document:

* `OpenCPI User Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_User_Guide.pdf>`_
  
* `OpenCPI Glossary <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Glossary.pdf>`_

Note that the *OpenCPI Glossary* is also contained in both the *OpenCPI Installation Guide* and the
*OpenCPI User Guide*.

This document assumes a basic understanding of the Linux command line (or "shell") environment.


Overview
--------

The Xilinx |device_name| is a Zynq UltraScale+ |device_type|.
See the |product_brief| for detailed information about the |device_name|.

In OpenCPI, the |device_name| is called the ``|platform_name|`` HDL platform.

Installation Prerequisites
--------------------------

The following items are required for OpenCPI |device_name| system installation and setup:

* Xilinx |device_name|

* 12V power supply

* micro-USB to USB-A cable

* micro-USB to female-USB-A adapter

* microSD card: use the one that comes with the product kit or provide a different one (OpenCPI |device_name| setup has been tested using a 16GB microSD card)


These items are typically provided with the |device_name| product kit.

.. note::

   Ignore any quick start cards or instructions that come with the product kit.
   This getting started guide and the `OpenCPI Installation Guide <https://opencpi.gitlab.io/releases/latest/docs/OpenCPI_Installation_Guide.pdf>`_
   provide the necessary instructions for setting up the |device_name| for OpenCPI.

The following items are optional:

* 1-Gigabit Ethernet cable

* Analog Devices FMCOMMS2 or FMCOMMS3 daughtercard

Installation Summary
--------------------

To set up the |device_name| system for OpenCPI development, perform the following steps:

* Configure the |device_name| for use with OpenCPI. See :ref:`|platform_name|-hw-setup`.

* (Optional) Connect an FMCOMMS2 or FMCOMMS3 daughtercard.  See :ref:`|platform_name|-hw-setup`.

* Install OpenCPI on the development host.  See :ref:`|platform_name|-enable-ocpi`.

* Prepare the development host to support the |device_name| system:

  * Connect a serial cable from the development host to the |device_name|.  See :ref:`|platform_name|-serial-console`.
    
  * Connect an SD card reader/writer to the development host. See :ref:`|platform_name|-sd-card-setup`.

  * (Optional) Connect the |device_name| to a Dynamic Host Configuration Protocol (DHCP)-supported network.  See :ref:`|platform_name|-net-mode`.

* Install the required vendor tools.  See :ref:`|platform_name|-vendor-tools`.

* Build the software and hardware platforms for the |device_name| system.  See :ref:`|platform_name|-ocpiadmin-install`.

* Prepare and write the bootable SD card for the |device_name| system.  See :ref:`|platform_name|-sd-card-setup`.

* Boot the |device_name| system and establish the OpenCPI environment on the system.  See :ref:`|platform_name|-cfg-runtime`.

* Run the OpenCPI-provided test application to confirm successful installation.  See :ref:`|platform_name|-run-test`.

